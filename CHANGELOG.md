# Change Log

All notable changes to the "zotero-for-typst" extension will be documented in this file.

## [0.1]

- Insert citation as `#cite(...)` command.
- Setting for host and port.
- Insert citation when typing `ZCT` (optional).