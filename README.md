# Zotero for Typst

Zotero integration for Typst file in VSCode. This requires the Better Bibtex plugin and a running Zotero instance.

## Features

- Insert `#cite(...)` style citation.